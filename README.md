### Profile

- 👋 Hi, I’m @Musicminion!Glad to see You on Github!
- 👀 I’m interested in Programing, webdesign and music composition,just as what my nickname told.
- 🌱 Special thanks to the course of the School of Software, Shanghai Jiaotong University, which provided scientific guidance for my programming study
- 💞️ I’m more looking to collaborate those who are responsible especially in a team work.In the meantime, I will also try my best!
- 📫 How to reach me? Please email at me via zhangziqian@sjtu.edu.cn(work) or report@takagi3.love(personal email)!
- Currently learning: ICS(Composition and System Softwore) Advanced Data Structure, OS, Software testing, Cloud OS, these repo will be public at the end of these semester

### Project 
- [KV-Store Database](https://github.com/Musicminion/2022-2023-2-Advanced-Data-Structure) (2023.3 A tiny C++ kv-store db using skip table and BF fliter, For course ADS)
- [E-Book](https://github.com/Musicminion/SJTU-SE2321-Web-Application-Development) (2022.3 ~ 2022.6 A React-Based and Spingboot Based Web Application, For course WebDEV)
- [E-Book Pro](https://github.com/Musicminion/SJTU-SE2321-Web-Application-Development) (2022.9 ~ 2022.12 A EBook with For High Performance Situation, using Redis, ElasticSearch...)

### Personal Link
- [Bilibili](https://space.bilibili.com/629072462)
- [水源社区](https://shuiyuan.sjtu.edu.cn/u/ayaka)
- [语雀文档](https://ayaka.yuque.com)
- [CSDN](https://www.csdn.net)

### Notes Project
Some personal notes has been published to website using [Docusaurus](https://github.com/facebook/docusaurus), you may need some tools to view this website since some network reasons. Don't use it for commercial use! Good luck to you for exams~
- [**Application-system-architecture Notes**](https://application-system-architecture.netlify.app/)(2022.9-2022.12)
- ICS notes (Still in work, come soon! Release before 2024.3)(2022.9-2023.6)
- Advanced Data Structure Notes (Still in work, come soon! Release before 2023.6)(2023.2-2023.6)
- OS notes (Still in work, come soon! Release before 2023.12)(2023.2-2023.6)

<!---
Musicminion/Musicminion is a ✨ special ✨ repository because its `README.md` (this file) appears on your GitHub profile.
You can click the Preview link to take a look at your changes.
--->
